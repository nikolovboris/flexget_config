## Instalace OSMC

1. Jít na stránku [OSMC](http://osmc.tv/)
2. Stáhnout a spustit instalátor
3. Vybrat poslední verzi pro RPI 3
4. Nastavit Wifi nebo Wired připojení
5. Nainstalovat na kartu
6. Po úspěšné instalaci dát kartu do Raspberry Pi a zapnout ho
7. Nastavit jazyk, časovou zónu, jméno zařízení, **zapnout SSH**, dokončit instalaci.
8. nainstalovat trak addon The `home screen-> Add-ons -> Download -> Program Add-ons-> Trakt-> Install` po instalaci dát nastavení jména a authorizace 




## Základní info
Pár údajů, které bude potřeba po instalaci

1. Zjistit jak se jmenuje disk na kterém budou data a **poznamenej si to**
2. Zjistit si IP adresu Raspberry Pi -> Setting -> System Info -> Summary -> IP Adress a **poznamenej si ji**
2. Vytvořit si účty na [addic7ed.com](www.addic7ed.com) a [opensubtitles.org](opensubtitles.org) se stejným uživatelským jménem a heslem
3. Jdi do MyOSMC > App Store - and install Transmission a Cron Task Scheduler
3. Jdi MyOSMC > Services, enable SSH and Cron.
4. Klikni na Apply a počkej než se doinstalují



## Připojení k RPI

Připoj se k Raspberry Pi pomocí Putty nebo Terminálu zadej `ssh osmc@<IP ADRESA PI>` heslo je `osmc`
Nepovinné: Zjistit veřejnou IP adresu  RPI


##Automatické načítání disku
Via: https://gist.github.com/etes/aa76a6e9c80579872e5f

1. Do terminálu zadat `sudo blkid` - zkopírovat UUID pro disk který budu automaticky chtít připojovat např. `/dev/sda2: LABEL="Data" UUID="5ACF-9563" TYPE="exfat" PARTLABEL="Data" PARTUUID="55cfbc26-b457-436e-a2f9-1cfd27a4f4e7"` tedy `5ACF-9563`
4. Vytvořím složku, do které se bude připojovat `sudo mkdir /mnt/<jméno disku>`
5. Nastavit práva `sudo chmod 770 /mnt/<jméno disku>`
6. `sudo mount /dev/sda2 /mnt/<jméno disku>` kde sda1 může být pro jiné označení
7. `sudo nano /etc/fstab`
	- pro EXFAT přidat následující řádek na konec souboru `UUID=5ACF-9563 /mnt/<jméno disku> exfat uid=1000,gid=1000,nofail,umask=007 0 0` `ctrl+x` pro ukončení `y` pro potvrzení Kde UUID je tvoje UUID
	- pro NTFS `sudo apt-get install ntfs-3g`
	- do fstabu přidat následující řádek na konec souboru `/dev/sda1 media/pi ntfs-3g auto,nofail,rw,default 0 0`
8. `sudo reboot` restart RPI
9.  Po restartu si zkontrolovat zda se disk automaticky načetl měl by být v cestě `/mnt/<název disku>`


##Nastavení struktury

```console
mkdir /mnt/<název disku>/TVshows
mkdir /mnt/<název disku>/Movies
mkdir /mnt/<název disku>/Downloads
mkdir /mnt/<název disku>/Downloads/tempmedia/
mkdir /mnt/<název disku>/Downloads/incomplete
```

např.

```console
mkdir /mnt/Data/TVshows
mkdir /mnt/Data/Movies
mkdir /mnt/Data/Downloads
mkdir /mnt/Data/Downloads/tempmedia/
mkdir /mnt/Data/Downloads/incomplete
```





## Trakt
1. V Traktu mít vytvořené listy `TVshows` a `Watchlist` a `trakt-series-begin`










Installation
-----------
1. nastavení locale `sudo dpkg-reconfigure locales` vybrat `en_GB.UTF-8 UTF-8`,
1. do `/etc/environment` přidat řádky `LC_ALL=en_GB.UTF-8
LANG=en_GB.UTF-8
LANGUAGE=en_GB.UTF-8
` potom `reboot`
1. Vyvořit složku `mkdir ~/flexget`
1. přejí do této složky `cd ~/flexget`
1. stáhnout si ní konfigurační soubor `wget https://bitbucket.org/nikolovboris/flexget_autosetup/raw/master/autosetup.sh`
2. upravit potřebné proměnné `nano autosetup.sh` doplnit název disku, trakt jméno, hesla
3. spustit `bash autosetup.sh`
1. Aktivovat trakt `~/flexget/bin/flexget trakt auth <účet>`
2. na stránce https://trakt.tv/activate/authorize zadat kód co se objeví v terminálu
3. ověřit, zda se opravdu flexget nainstaloval `~/flexget/bin/flexget -V`
3. pro spuštění flexgetu stačí zadat `~/flexget/bin/flexget execute --now`



Titulky
-----------
1. registrovat se na [Opensubtitles](https://www.opensubtitles.org/cs/newuser)
1. v Kodi `Addons -> Download -> Subtitles -> Opensubtitles` a nainstalovat, následně je potřeba se přihlásit pomocí vytvořeného účtu



Folder structure:
----------------
```
/mnt/                      (root)
............/Downloads
....................../tempmedia     (downseries + movies)
............/TVshows               (series)
............/Movies               (movies)
```
Transmission:
----------------
http://<ip adresa PI>:9091/transmission/web/